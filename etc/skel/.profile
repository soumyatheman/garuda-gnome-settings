export AMD_VULKAN_ICD=RADV
export EDITOR=/usr/bin/micro
export BROWSER=firedragon
export TERM=gnome-terminal
export MAIL=geary
export QT_QPA_PLATFORMTHEME="gnome"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
